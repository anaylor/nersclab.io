# Community Showcase

This section highlights approaches or techniques developed by other
Spin users.

If you'd like to share something you've done with Spin that you think
other users would find helpful, we'd love to add your work to this
showcase. Contact us by [submitting a ticket](https://help.nersc.gov/).

We also encourage you to interact with other Spin users in the #spin
channel of the [NERSC Users Slack workspace](https://www.nersc.gov/users/NUG/nersc-users-slack/).
