#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <string.h>
#include <time.h>
#include <math.h>

int main(int argc,char *argv[])
{
  // Define Variables
double begin, end;
double time_spent;
begin=omp_get_wtime();
int **x, *y,*A;
int n=atoi(argv[1]);
int i,j;
y=(int *)malloc(n*sizeof(int));
A=(int *)malloc(n*sizeof(int));
x=(int **)malloc(n*sizeof(int*));
for(i=0;i<n;i++)
x[i]=(int *)malloc(n*sizeof(int));
for (i=0; i<n; i++) {
    for (j=0; j<n; j++)
                   x[i][j]= i%10;
    }
for (i=0; i<n; i++) {
        y[i]= i%10;
        A[i] = 0;
  }
for (i=0; i<n; i++) {
    for (j=0; j<n; j++)
      A[i] += x[i][j] * y[j];
}
end=omp_get_wtime();
time_spent = (double)(end - begin);
printf("Time=%lf",time_spent);
}
